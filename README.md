# Backbase Front End Assignment: Make A Transaction

## Storage
I'm currently using LocalStorage (https://www.w3schools.com/html/html5_webstorage.asp) for storing the data with no expiration date on the web browser. You may try realoading/closing the page, tab, or web-browser (non-incognito); you may still be able to see all the transactions.

## CSS Framework
I've decided on using bootstrap 4.0 since it is widely being used, well-maintained, has a ripe-community for support, and most of all it is open-source.

## Extra Framework
I've utilized jQuery-3.4.0 for faster DOM manipulation.

## Intricates
I've encountered a well known issue on AngularJS for the sorting of numerical values (float in particular);
I still continued on using AngularJS since there are some features that are very usable on this activity (like the ng-repeat).
It saves time in rendering repetitive displays, easy-to-use, and very dynamic.