var app = angular.module('main_app',[]);
	
//set custom service..	
app.factory('storageService', ['$http',function($http){
	return {
		setInitJson : function (key, json) {
			if (!key) { return; }
			if (typeof json === "object") { 
				value = JSON.stringify(json); 
			} 
			return localStorage.setItem(key, value) ? true : false;
		},

		getItem : function (key) {
			//Retreive Storage 
			return JSON.parse(localStorage.getItem(key));
		},

		clear: function () {
			// this will clear all;
			return localStorage.clear() ? true : false;
		}
	};
}]);

// main app controller
app.controller('maincontroller', ['$scope', 'storageService', function($scope, storageService) {
	
	// from user account balance..
	$scope.balance = storageService.getItem('owner_balance').balance;
	$scope.transactions = storageService.getItem('transactions_table');
	$scope.merchants = storageService.getItem('merchants_table');
	$scope.account_value = "Free Checking(4692) $" + parseFloat($scope.balance).toFixed(2); 
	$scope.inputdisabled = true;
	$scope.dates = [];
	
	$scope.set_color = function (value) {
		return { borderLeft : value + ' solid 5px' }
	}

	angular.forEach($scope.transactions, function (data) {
		$scope.dates.push(data.transactionDate);
	});

	$scope.autoComplete = function (value) {
		var merchants = [];
		angular.forEach($scope.merchants, function (val, key) {
			if(key.toLowerCase().indexOf(value.toLowerCase())>=0){
				merchants.push(key);
			}

			if (value == "") {
				merchants = [];
				$scope.merchantlist = null;
			}
		});

		$scope.merchantlist = merchants;
	}

	$scope.fillInputs = function(value){
		$scope.to_account = value;
		$scope.merchantlist = null;
	}

	$scope.setOrderProperty = function(propertyName) {
        if ($scope.orderProperty === propertyName) {
            $scope.orderProperty = '-' + propertyName;
        } else if ($scope.orderProperty === '-' + propertyName) {
            $scope.orderProperty = propertyName;
        } else {
            $scope.orderProperty = propertyName;
        }
    };

	$scope.setOrderProperty('transactionDate');
}]);

// loader, run in first load..
app.run(['storageService', function(storageService){
	if (!("transactions_json" in localStorage)) {
		storageService.setInitJson('transactions_json', transactions_json);
	}

	if (!("merchants_table" in localStorage)) {
		var _merchants = [];
		angular.forEach(transactions_json.data, function (value, key) {
			if (_merchants.indexOf(value.merchant) <= 0) {
				_merchants[value.merchant] = {
					name: value.merchant,
					logo: value.merchantLogo,
					categoryCode: value.categoryCode,
					transactionType: value.transactionType,
				};
			}
		});

		storageService.setInitJson('merchants_table', Object.assign({}, _merchants));
	}
	
	// store to localstorage
	if (!("transactions_table" in localStorage)) {
		var _transactions = [];
		angular.forEach(transactions_json.data, function (value, key) {
			_transactions.push({
				merchant: value.merchant,
				transactionDate: value.transactionDate,
				amount: parseFloat(value.amount).toFixed(2),
			});
		});

		storageService.setInitJson('transactions_table', _transactions);
	}

	// store to localstorage
	if (!("owner_balance" in localStorage)) {
		var _debit = parseFloat("0.00");

		angular.forEach(storageService.getItem('transactions_table'), function (value, key) {
			_debit += parseFloat((value.amount).toString());
		});

		var _balance = (parseFloat("5824.56").toFixed(2) - parseFloat(_debit).toFixed(2));
		storageService.setInitJson('owner_balance', {'balance':_balance});
	}
}]);



$(function() {

	$('input[name="merchant"]').on('change', function(e) {
		$(this).removeClass('border-danger');
	});

	$('input[name="amount"]').on('change', function(e) {
		$(this).val(parseFloat(this.value).toFixed(2));
	});

	$("#transaction_form").on('submit', function(e) {
		e.preventDefault();
		var _data = {};
		_balance = JSON.parse(localStorage.getItem("owner_balance"));
		_merchants = JSON.parse(localStorage.getItem("merchants_table"));
		_merchant_el = $('input[name="merchant"]');

		if (Object.keys(_merchants).indexOf($(_merchant_el).val()) >= 0) {
			$(_merchant_el).removeClass('border-danger');
		} else {
			alert('"' + $(_merchant_el).val() + '" is not included on the Merchant List.')
			$(_merchant_el).addClass('border-danger');
			return;
		}

		$(this).find('input:required').each(function(key, val) {
			_data[$(val).attr('name')] = $(val).val();
		});

		_data['transactionDate'] = Date.parse(new Date);

		if (parseFloat(_balance.balance).toFixed(2) < parseFloat(_data['amount']).toFixed(2) || _balance.balance <= 0) {
			alert('Insufficient Balance.')
			return;
		}

		_transactions = JSON.parse(localStorage.getItem("transactions_table"));
		_transactions.unshift(_data);
		localStorage.setItem('transactions_table', JSON.stringify(_transactions));

		_balance = (parseFloat(_balance.balance).toFixed(2) - parseFloat(_data['amount']).toFixed(2));
		localStorage.setItem('owner_balance', JSON.stringify({balance:_balance}));

		var scope = angular.element("#controller").scope();
            scope.transactions = _transactions;
            scope.account_value = "Free Checking(4692) $" + parseFloat(_balance).toFixed(2);
            scope.$apply();

		$(this).find('input:required').each(function(key, val) {
			$(val).val('');
		});
	});

});